3.times do |topic|
  Topic.create!(
    title: "Topic #{topic}"
  )
end

puts "3 Topics created"


10.times do |blog|
  Blog.create!(
    title: "My Blog Post #{blog}",
    body: "I learned that courage was not the absence of fear, but the triumph over it. The brave man is not he who does not feel afraid, but he who conquers that fear.",
    topic_id: Topic.last.id
  )
end

puts "10 blog posts created"

5.times do |skill|
  Skill.create!(
    title: "Rails #{skill}",
    percent_utilized: 15,
  ) 
end

puts "5 skills created"

8.times  do |portfolio_item|
  Portfolio.create!(
    title: "Portifolio title: #{portfolio_item}",
    subtitle: "Ruby on Rails",
    body: "If you talk to a man in a language he understands, that goes to his head. If you talk to him in his language, that goes to his heart.",
    main_image: "http://via.placeholder.com/600x400",
    thumb_image: "http://via.placeholder.com/350x200",
  )
end

1.times  do |portfolio_item|
  Portfolio.create!(
    title: "Portifolio title: #{portfolio_item}",
    subtitle: "Angular",
    body: "If you talk to a man in a language he understands, that goes to his head. If you talk to him in his language, that goes to his heart.",
    main_image: "http://via.placeholder.com/600x400",
    thumb_image: "http://via.placeholder.com/350x200",
  )
end

puts "9 portifolio items created"

3.times do |technology|
  Portfolio.last.technologies.create!(
    name: "Technology #{technology}",
  )
end

puts "3 technologies created"
